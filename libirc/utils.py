def hook(hook):
    def wrapped(function):
        if not hasattr(function, "hook"):
            function.hook = []
        function.hook.append(hook)
        return function

    return wrapped


def hook_first(hook):
    def wrapped(function):
        if not hasattr(function, "hook_first"):
            function.hook_first = []
        function.hook_first.append(hook)
        return function

    return wrapped


def hook_last(hook):
    def wrapped(function):
        if not hasattr(function, "hook_last"):
            function.hook_last = []
        function.hook_last.append(hook)
        return function

    return wrapped
